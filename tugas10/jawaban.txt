Soal 1 Membuat Database
create database myshop

soal 2 Membuat Table di Dalam Database
a. table user
create table users(
    id int(8) primary_key autoincrement,
    name varchar(255),
    email varchar(255),
    password varchar(255)
);

b. table categories
create table users(
    id int(8) primary_key autoincrement,
    name varchar(255)
);

c. table items
create table users(
    id int(8) primary_key autoincrement,
    name varchar(255),
    description varchar(255),
    price int(25),
    price int(25),
    category_id int(25),
    foreign key(category_id) references categories(id)
);


soal 3 memasukan data
a. data user
INSERT INTO users (name, email, password)
VALUES ( "John Doe", "john@doe.com", "john123" ),
( "John Doe", "john@doe.com", "jenita123" )

b. data categories
INSERT INTO categories (name)
VALUES ("gadget"),("cloth"), ("men"), ("women"), ("branded")

c. data items
INSERT INTO items (name, description, price, stock, category_id)
VALUES
("Sumsang b50", "hape keren dari merek sumsang",	4000000,	100,	1),
("Uniklooh",	"baju keren dari brand ternama",	500000,	50,	2),
("IMHO Watch",	"jam tangan anak yang jujur banget",	2000000,	10,	1)

Soal 4 Mengambil Data dari Database
a. menggambil data users tanpa password
SELECT name, email FROM `users`

b. Mengambil data items
- SELECT * FROM `items` WHERE price>1000000
- SELECT * FROM `items` WHERE name LIKE '%sang%'

c. Menampilkan data items join dengan kategori
- SELECT items.name, items.description, items.description, items.price, items.category_id, categories.name as category FROM items INNER JOIN categories ON items.category_id = categories.id

Soal 5 Mengubah Data dari Database
- UPDATE items set price = 2500000 WHERE id=1;