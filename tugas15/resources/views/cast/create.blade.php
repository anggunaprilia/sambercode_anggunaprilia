@extends('layout.master')

@section('judul')
    Cast Create
@endsection

@section('content')

<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Cast</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="/cast" method="POST">
        @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="Nama">Nama</label>
          <input type="text" class="form-control" id="nama" name="nama" placeholder="">
        </div>
        <div class="form-group">
          <label for="Umur">Umur</label>
          <input type="number" class="form-control" id="umur" name="umur" placeholder="">
        </div>
        <div class="form-group">
            <label for="Bio">Bio</label>
            <input type="text" class="form-control" id="bio" name="bio" placeholder="">
          </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
@endsection