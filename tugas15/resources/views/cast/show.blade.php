@extends('layout.master')

@section('judul')
    Show Cast
@endsection

@section('content')

<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Show Cast</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="/cast" method="POST">
        @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="Nama">Nama : </label>
          {{$cast->nama}}
        </div>
        <div class="form-group">
          <label for="Umur">Umur : </label>
          {{$cast->umur}}
        </div>
        <div class="form-group">
            <label for="Bio">Bio : </label>
            {{$cast->bio}}
          </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <a href="/cast" class="btn btn-primary my-1">kembali</a>
      </div>
    </form>
  </div>
@endsection