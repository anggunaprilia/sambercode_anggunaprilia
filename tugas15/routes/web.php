<?php

use App\Http\Controllers\CastController;
use App\Http\Controllers\RegisterController;
use Illuminate\Support\Facades\Route;
use Spatie\FlareClient\View;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('halamanUtama');
});

Route::get('/index', [RegisterController::class, 'index']);
Route::get('/register', [RegisterController::class, 'regis']);
Route::post('/kirim', [RegisterController::class, 'proses']);

Route::get('/master', function(){
    return View("layout.master");
});

Route::get('/table', function(){
    return View("table");
});

Route::get('/data-table', function(){
    return View("data-table");
});

Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/create', [CastController::class, 'create']);
Route::post('/cast', [CastController::class, 'store']);
Route::get('/cast/{cast_id}', [CastController::class, 'show']);
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{cast_id}', [CastController::class, 'update']);
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);