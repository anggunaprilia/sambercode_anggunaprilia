<?php
require_once "animal.php";
require_once "ape.php";
require_once "frog.php";


$sheep = new Animal("shaun");

echo "name : $sheep->name " . PHP_EOL; // "shaun"
echo "legs: $sheep->legs " . PHP_EOL; // 4
echo "cold blooded : $sheep->cold_blooded" . PHP_EOL; // "no"


// index.php
$sungokong = new Ape("kera sakti");
echo "name : $sungokong->name " . PHP_EOL;
echo "legs: $sungokong->legs " . PHP_EOL;
echo "cold blooded : $sungokong->cold_blooded" . PHP_EOL;
$sungokong->yell(). PHP_EOL; // "Auooo"

$kodok = new Frog("buduk");
echo "name : $kodok->name " . PHP_EOL;
echo "legs: $kodok->legs " . PHP_EOL;
echo "cold blooded : $kodok->cold_blooded" . PHP_EOL;
$kodok->jump() . PHP_EOL ; // "hop hop"