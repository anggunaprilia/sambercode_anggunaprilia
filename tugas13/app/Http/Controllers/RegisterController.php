<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function index(){
        return view("halamanUtama");
    }

    public function regis(){
        return view("register");
    }

    public function proses(Request $request){
        $fname = $request['fname'];
        $lname = $request['lname'];

        return view('home', ['fname' => $fname, 'lname' =>$lname]);
    }

}
